import {useState} from 'react';
import {View, Text, TextInput, Button, StyleSheet, ScrollView} from 'react-native';

const List = () => {
    const [enteredGoalText, setEnteredGoalText] = useState<string>('');
    const [courseGoals, setCourseGoals] = useState<string[]>([]);

    const onTextChangeHandler = (value: string) => {
        setEnteredGoalText(value);
    };

    const addChangehandler = () => {
        setCourseGoals((currentCourse) => [...currentCourse, enteredGoalText]);
    };

    return (
        <View style={styles.container}>
            <View style={styles.inputTextContainer}>
                <TextInput
                    style={styles.inputText}
                    placeholder="Your Course Goal"
                    onChangeText={onTextChangeHandler}
                />
                <Button title="Add Goal" onPress={addChangehandler} />
            </View>
            <View style={styles.goalContainer}>
                <ScrollView alwaysBounceVertical={false}>
                    {courseGoals.map((goal, index) => (
                        <View style={styles.goalitem} key={index}>
                            <Text style={styles.goalText}>{goal}</Text>
                        </View>
                    ))}
                </ScrollView>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 50,
        paddingHorizontal: 16
    },
    inputTextContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 24,
        borderBottomWidth: 1,
        borderBottomColor: '#cccccc'
    },
    inputText: {
        borderWidth: 1,
        borderColor: '#cccccc',
        width: '70%',
        marginRight: 8,
        padding: 8
    },
    goalContainer: {
        flex: 5
    },
    goalitem: {
        margin: 8,
        padding: 8,
        borderRadius: 6,
        backgroundColor: '#5e0acc'
    },
    goalText: {
        color: 'white'
    }
});

export default List;
